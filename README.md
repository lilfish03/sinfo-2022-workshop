# From development to production, an introduction on how to test and deploy using CI/CD"
This workshop was created to integrate the [SINFO](https://sinfo.org/) conference (2022 edition).  
The goal of this workshop is to give an overview of how code is usually developed, tested and deployed in enterprises.  
As an example we will develop a CI/CD pipeline to test a simple web application and deploy it on [Heroku cloud platform](https://www.heroku.com/what).

You can follow the presentation here: [SINFO22 - CI/CD Workshop](https://docs.google.com/presentation/d/1hl2teqO5GuCVjQxefBqoVZQ1IRSnIrM03nU7-xhJQXQ/edit?usp=sharing)

# Requirements
- Basic understanding of **Unix**, **Git** and **Python**
- **Gitlab** and **Heroku** accounts
- **Git** installed
- (OPTIONAL) **Python3**, **Pip** and **VirtualEnv** installed
  - In this workshop we will not focus on setting up a **local environment** to work on the "Calculator" app, but if you are interested in doing it, just follow the instructions in [Setting Up Local Environment](docs/setting-up-local-env.md)

# Workshop

This workshop is splitted into various parts:
1. [Getting started](#1---getting-started)
2. [Testing](#2---testing)
3. [Linting](#3---linting)
4. [Deploying to Heroku](#4---deploying-to-heroku)
5. [Building](#5---building)

# 1 - Getting started

In this section we will fork the `sinfo-2022-workshop` and manually trigger a GitLab CI/CD pipeline.

**You will need:**
- [GitLab](https://gitlab.com/users/sign_in) account
- [Git](https://git-scm.com/) installed

## 1.1 - Fork the SINFO2022 project

Access [https://gitlab.com/singlestore-public/sinfo-2022-workshop/-/forks/new](https://gitlab.com/singlestore-public/sinfo-2022-workshop/-/forks/new) and `Fork` the project into your own GitLab account:

```
# Fork the sinfo2022 project https://gitlab.com/singlestore-public/sinfo-2022-workshop/-/forks/new
```

After being redirected to the forked project (**your own project copy**), click on `Clone` to get the Git repo URL and clone it to your machine:

```
# Clone your own forked repository
$ git clone <YOUR_OWN_REPO_URL>
$ cd sinfo-2022-workshop
```

## 1.2 - Activate Shared Runners

On your Gitlab project, navigate to `Settings > CI/CD > Runners` and `Enable shared runners for this project`. If you are asked to provide a credit card number in order to validate your account, just use the one provided to you during the workshop.

## 1.3 - Trigger a CI/CD pipeline

Navigate to `CI/CD > Pipelines`, and click on `Run pipeline` to manually trigger a pipeline.

Check the `.gitlab-ci.yml` that already exists in the project, this is where the GitLab CI is configured.

Click on a pipeline Job and verify that the `echo` messages defined on `.gitlab-ci.yml` were printed to the log.

> **NOTE:** For now these Jobs are only printing some messages, we will change them later in this workshop.

# 2 - Testing

In this section we will continue using the [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/) to test our app.
  
## 2.1 - Run Tests

Adapt the existing `unit-test-job` Job to execute the unit tests by adding the following lines to the `script` section on the `.gitlab-ci.yml` file:

```
    - python3 -m venv env
    - source env/bin/activate
    - pip install -r requirements.txt
    - pytest
```

Then commit and push your changes to trigger a CI/CD pipeline:

```
$ git add .gitlab-ci.yml
$ git commit -m "Fix 'unit-test-job' Job"
$ git push origin main:main
```

After pushing your changes, a **new pipeline was automatically triggered**, navigate to `CI/CD > Pipelines` and **check the status of that pipeline**.

## 2.2 - Fix the failing test
One test is failing, try to understand the failure and fix the code in `calculator.py` to make it pass.

After fixing the test, commit and push your fix:

```
$ git add calculator.py
$ git commit -m "Fix test"
$ git push origin main:main
```

Verify that pipeline is now "green".

# 3 - Linting

In this section we will use the [Pylint](https://pylint.org/) for static code analysis.

## 3.1 - Run Lint

Create a new `lint-test-job` job that executes on the `test` stage.

In order to run linting, the `lint-test-job` should contain the following `script`:

```
    - python3 -m venv env
    - source env/bin/activate
    - pip install -r requirements.txt
    - pylint *.py
```

Commit and push your changes to trigger a new pipeline:

```
$ git add .gitlab-ci.yml
$ git commit -m "Add lint job"
$ git push origin main:main
```

Verify that a new job exists and **check the pipeline status**.

## 3.2 - Fix Lint

Lint is failing, try to understand the failure and fix the code in `calculator.py` and `calculator_test.py` to make it pass.

After fixing the lint, commit and push your fix:

```
$ git add calculator.py
$ git add calculator_test.py
$ git commit -m "Fix lint"
$ git push origin main:main
```

Verify that pipeline is now "green".

# 4 - Deploying to Heroku

In this section we will deploy our "Calculator" app to Heroku, a platform as a service (PaaS) that allows us to run our app in the cloud.

**You will need:**
- [Heroku](https://signup.heroku.com/login) account
- Heroku uses the **Procfile** and **requirements.txt** already present in the repository to know what to run and what dependencies to install

## 4.1 - Create an Heroku app

Access https://dashboard.heroku.com/new-app, choose the region and click on `Create app` (a random name will be generated if none is introduced).

Your application will be accessible on [https://<APP_NAME>.herokuapp.com](https://<APP_NAME>.herokuapp.com) (replace the `<APP_NAME>` with the name of the heroku app that was created).

## 4.2 - Get API key

Access your account setting https://dashboard.heroku.com/account to get the API key.

## 4.3 - Configure CI/CD Variables

Navigate to `Settings > CI/CD > Variables` to define the following environment variables:

- `HEROKU_API_KEY`
- `HEROKU_APP_NAME`

You should use the app name and API key from previous steps.

## 4.4 - Disable Git Shallow Clone

Navigate to `Settings > CI/CD > General pipelines` and change the `Git shallow clone` to `0` (this will disable the Git shallow clone).

## 4.5 - Adapt the `deploy-job` Job

Adapt the existing `deploy-job` Job to deploy the app to Heroku, by adding the following to the job `script`:

```
    - git remote add heroku https://heroku:${HEROKU_API_KEY}@git.heroku.com/${HEROKU_APP_NAME}.git
    - git push -f heroku HEAD:refs/heads/master
```

Commit and push your changes to trigger a new pipeline:

```
$ git add .gitlab-ci.yml
$ git commit -m "Auto deploy"
$ git push origin main:main
```

Check the `deploy-job` Job logs to verify that the app was deployed to heroku.

## 4.6 - Access the app

Access [https://<APP_NAME>.herokuapp.com](https://<APP_NAME>.herokuapp.com) and verify that the "Calculator" app is available (replace the `<APP_NAME>` with the name of the heroku app that was created).

# 5 - Building

In this section we will create a Job that replaces the `@BUILD_USER@` and `@BUILD_TIME@` displayed in the UI with correct values.

## 5.1 - Create the `build-job` Job

This new `build-job` Job should belong to a new `build` stage that executes before the `test` stage.

Note that if a `build.ini` file exists, the "Calculator" app will read it. Check the `build.ini.sample` sample to understand how to define build variables.

The `build-job` must create the `build.ini`:

```
    - cp build.ini.sample build.ini
    - sed -i "s/@BUILD_USER@/${GITLAB_USER_NAME}/g" build.ini
    - sed -i "s/@BUILD_TIME@/$(date)/g" build.ini
```

> **TIP:** `${GITLAB_USER_NAME}` is a Gitlab pre-defined variable, check see https://docs.gitlab.com/ee/ci/variables/predefined_variables.html for a full list of pre-defined variables

Now that the `build-job` Job is creating the `build.ini` file, we need to make it available to the `deploy-job`. The way to pass files from one job to another is by using [artifacts](https://docs.gitlab.com/ee/ci/yaml/index.html#artifactspaths). Add the following to the `build-job` in order to generate the artifact.

```
  artifacts:
    paths:
      - build.ini
```

Commit and push your changes to trigger a new Pipeline:

```
$ git add .gitlab-ci.yml
$ git commit -m "Add 'build-job' Job"
$ git push origin main:main
```

Check if the artifact was correctly generated by using the `Download` or `Browse` button that will be available in the `build-job` Job page.

## 5.2 - Adapt the `deploy-job` Job

Now we need to adapt the `deploy-job` Job to get the artifact. This is done by using Job [dependencies](https://docs.gitlab.com/ee/ci/yaml/index.html#dependencies). Add the following to the `deploy-job`:

```
  dependencies:
    - build-job
```

Now the `deploy-job` already has access to the `build.ini` file, so the only thing that is missing is adding this file to the local Git repo before pushing to the `heroku` remote. Add the following to the `deploy-job` Job `script`:

```
- git add "build.ini"
- git commit -m "Adding 'build.ini'"
```

Commit and push your changes to trigger a new Pipeline:

```
$ git add .gitlab-ci.yml
$ git commit -m "Complete 'deploy-job' Job"
$ git push origin main:main
```

Access the Heroku app [https://<APP_NAME>.herokuapp.com](https://<APP_NAME>.herokuapp.com) and verify that the footer is now displaying the correct "**build user**" and "**build time**" (replace the `<APP_NAME>` with the name of the heroku app that was created).

## Extra Challenges
If you have reached this far with time to spare, there are a couple of extra objectives you can try to accomplish to improve your CI/CD. Take a look at [Extra Challenges](docs/extra-challenges.md).
